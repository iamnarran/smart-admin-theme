import React from 'react'

import { Stats, BigBreadcrumbs, WidgetGrid, JarvisWidget } from '../../../components'

import DropzoneInput from '../../../components/forms/inputs/DropzoneInput'
import Datatable from '../../../components/tables/Datatable'


export default class DropzoneDemo extends React.Component {
    render() {
        return (
            <div id="content">

                <WidgetGrid>
                    <div className="row">
                        <article className="col-sm-12">
                            <JarvisWidget editbutton={false} color="darken">
                                <header>
                                    <span className="widget-icon"><i className="fa fa-table" /></span><h2>Байгууллагын мэдээлэл</h2>
                                </header>
                                <div>
                                    <div className="widget-body no-padding">
                                        <Datatable
                                            options={{
                                                ajax: 'assets/api/tables/datatables.standard.json',
                                                columns: [{ data: "id" }, { data: "image" }, { data: "name" }, { data: "company" }, 
                                                            { data: "check" }, { data: "status" }, { data: "bDay" }, { data: "date" }, { data: "uildel"}]
                                            }}
                                            paginationLength={true} 
                                            className="table table-striped table-bordered table-hover"
                                            width="100%">
                                            <thead>
                                                <tr>
                                                    <th data-hide="phone">ID</th>
                                                    <th data-class="expand"><i className="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs" /> Зураг </th>
                                                    <th data-hide="phone"><i className="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs" /> Бренд нэр </th>
                                                    <th>Company</th>
                                                    <th data-hide="phone,tablet"><i className="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs" /> Зөвхөн Имарт бренд эсэх </th>
                                                    <th data-hide="phone,tablet">Төлөв</th>
                                                    <th data-hide="phone,tablet">Бүртгэсэн</th>
                                                    <th data-hide="phone,tablet"><i className="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs" /> Бүртгэгдсэн огноо </th>
                                                    <th data-hide="phone,tablet">Үйлдэл</th>
                                                </tr>
                                            </thead>
                                        </Datatable></div>
                                </div>
                            </JarvisWidget>
                        </article>


                    </div>

                </WidgetGrid>
            </div>
        )
    }
}