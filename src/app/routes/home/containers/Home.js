/**
 * Created by griga on 11/30/15.
 */

import React, {PropTypes} from 'react'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios'
import WidgetGrid from '../../../components/widgets/WidgetGrid'
import JarvisWidget  from '../../../components/widgets/JarvisWidget'
import {checkToken} from '../../../components/account/accountActions';

class Home extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  static contextTypes = {
    router: PropTypes.object
  }

  componentWillReceiveProps(nextProps) {
  }

  componentWillMount() {
    console.log("WILL MOUNT")
    let token = localStorage.getItem('token');

    if(token !== null){
      console.log("WE'VE TOKEN", token);
      this.props.actions.checkToken(token)
    }
    else{
      if (this.props.user.user.email === undefined) {
        this.context.router.push('/login');
      }
    }
  }

  render() {
    console.log(this.props)
    return (
      <div id="content">
        <WidgetGrid>
          <div className="row">
            <article className="col-sm-6">
            </article>
          </div>
        </WidgetGrid>
      </div>
    )
  }

}

function mapStateToProps(state, ownProps) {
  return {
    user: state.account
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({checkToken}, dispatch)
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Home);
