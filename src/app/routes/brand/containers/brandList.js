import React from 'react'

import { Stats, BigBreadcrumbs, WidgetGrid, JarvisWidget } from '../../../components'

import Datatable from '../../../components/tables/Datatable'


export default class DropzoneDemo extends React.Component {
    render() {
        return (
            <div id="content">

                <WidgetGrid>
                    <div className="row">
                        <article className="col-sm-12">
                            <JarvisWidget editbutton={false} color="darken">
                                <header>
                                    <span className="widget-icon"><i className="fa fa-table" /></span><h2>Брендийн жагсаалт</h2>
                                </header>
                                <div>
                                    <div className="widget-body no-padding">
                                        <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style={{ float: "right", margin: "10px" }}>Татах/excel</button>
                                        <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style={{ float: "right", margin: "10px" }}>Нэмэх</button>
                                        <Datatable
                                            options={{
                                                ajax: 'assets/api/tables/datatables.standard.json',
                                                columns: [{ data: "id" }, { data: "image" }, { data: "name" }, { data: "company" },
                                                { data: "check" }, { data: "status" }, { data: "bDay" }, { data: "date" }, { data: "uildel" }]
                                            }}
                                            paginationLength={true}
                                            className="table table-striped table-bordered table-hover"
                                            width="100%">
                                            <thead>
                                                <tr>
                                                    <th data-hide="phone">ID</th>
                                                    <th data-class="expand"><i className="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs" /> Зураг </th>
                                                    <th data-hide="phone"><i className="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs" /> Бренд нэр </th>
                                                    <th>Company</th>
                                                    <th data-hide="phone,tablet"><i className="fa fa-fw fa-map-marker txt-color-blue hidden-md hidden-sm hidden-xs" /> Зөвхөн Имарт бренд эсэх </th>
                                                    <th data-hide="phone,tablet">Төлөв</th>
                                                    <th data-hide="phone,tablet">Бүртгэсэн</th>
                                                    <th data-hide="phone,tablet"><i className="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs" /> Бүртгэгдсэн огноо </th>
                                                    <th data-hide="phone,tablet">Үйлдэл</th>
                                                </tr>
                                            </thead>
                                        </Datatable></div>
                                </div>
                            </JarvisWidget>
                        </article>
                    </div>
                </WidgetGrid>
                <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h5 className="modal-title" id="exampleModalLabel">Баннер нэмэх</h5>
                            </div>

                            <div>
                                <BootstrapValidator options={validatorOptions}>

                                    <form id="buttonGroupForm" onSubmit={this.onSubmit} className="form-horizontal">
                                        <label></label>
                                        <fieldset>
                                            <div className="form-group">
                                                <label className="col-xs-2 col-lg-3 control-label">Бренд нэр*: </label>
                                                <div className="col-xs-9 col-lg-6 inputGroupContainer">
                                                    <div className="input-group">
                                                        <input type="text" className="form-control" name="amount" />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <div className="form-group">
                                                <label className="col-xs-2 col-lg-3 control-label">Зураг: </label>
                                                <div className="col-xs-9 col-lg-9 inputGroupContainer">
                                                    <input type="file" className="btn btn-default" id="exampleInputFile1" />
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset>
                                            <div className="form-group">
                                                <label className="col-xs-2 col-lg-3 control-label"></label>
                                                <div className="col-xs-9 col-lg-9 inputGroupContainer">
                                                    <div className="checkbox">
                                                        <label>
                                                            <input type="checkbox" className="checkbox style-0" />
                                                            <span>Тогтмол ашиглах баннер бол тэмдэглэ.</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                    </form>
                                </BootstrapValidator>
                            </div>
                            <div className="modal-footer">
                                <div className="col-md-12">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal"> Цуцлах </button>
                                    <button className="btn btn-primary" type="submit"> Хадгалах </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}