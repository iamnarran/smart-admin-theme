import * as actions from "./menuActions";

const initialState = {
  list: []
};

export default function menuReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_MENU_LIST:
      console.log(action.payload)
      return { ...state, list: action.payload };
    default:
      return state;
  }
}
