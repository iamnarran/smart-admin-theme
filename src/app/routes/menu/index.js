
export default {
  component: require('../../components/common/Layout').default,
  childRoutes: [
    {
      path: 'menu',
      getComponent(nextState, cb){
        System.import('./containers/menuList').then( m => {
          cb(null, m.default)
        })
      }
    },
  ]
};