import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  Stats,
  BigBreadcrumbs,
  WidgetGrid,
  JarvisWidget
} from "../../../components";
import BootstrapValidator from "../../../components/forms/validation/BootstrapValidator";

import Datatable from "../../../components/tables/Datatable";
import {getMenuList} from '../menuActions';

const validatorOptions = {
  excluded: ":disabled",
  feedbackIcons: {
    valid: "glyphicon glyphicon-ok",
    invalid: "glyphicon glyphicon-remove",
    validating: "glyphicon glyphicon-refresh"
  },
  fields: {
    gender: {
      validators: {
        notEmpty: {
          message: "The gender is required"
        }
      }
    },
    "languages[]": {
      validators: {
        choice: {
          min: 1,
          max: 2,
          message: "Please choose 1 - 2 languages you can speak"
        }
      }
    }
  }
};

class menuList extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      items: []
    };
  }

  componentWillMount() {
      console.log("willMount")
      this.props.getMenuList()
  }
  render() {
    return (
      <div id="content">
        <WidgetGrid>
          <div className="row">
            <article className="col-sm-12">
              <JarvisWidget editbutton={false} color="darken">
                <header>
                  <span className="widget-icon">
                    <i className="fa fa-table" />
                  </span>
                  <h2>Меню жагсаалт</h2>
                </header>
                <div>
                  <div className="widget-body no-padding">
                    <button
                      type="button"
                      className="btn btn-primary"
                      data-toggle="modal"
                      data-target="#downloadModal"
                      style={{ float: "right", margin: "10px" }}
                    >
                      Татах/excel
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      data-toggle="modal"
                      data-target="#newModal"
                      style={{ float: "right", margin: "10px" }}
                    >
                      Нэмэх
                    </button>
                    <Datatable
                      options={{
                        data: this.props.menu.list,
                        // ajax: "assets/api/tables/datatables.standard.json",
                        columns: [
                          { data: "menuid" },
                          { data: "menunm"},
                          { data: "link" },
                          { data: "order" },
                          { data: "status" },
                        //   {}
                        ]
                      }}
                      paginationLength={true}
                      className="table table-striped table-bordered table-hover"
                      width="100%"
                    >
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Гарчиг</th>
                          <th>Холбоос</th>
                          <th>Order</th>
                          <th>Төлөв</th>
                          {/* <th>Засварлах</th> */}
                        </tr>
                      </thead>
                    </Datatable>
                  </div>
                </div>
              </JarvisWidget>
            </article>
          </div>
        </WidgetGrid>
        <div
          className="modal fade"
          id="newModal"
          role="dialog"
          aria-labelledby="newModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <h5 className="modal-title" id="newModalLabel">
                  Меню нэмэх
                </h5>
              </div>

              <div>
                {/* <BootstrapValidator options={validatorOptions}> */}
                  <form
                    id="buttonGroupForm"
                    onSubmit={this.onSubmit}
                    className="form-horizontal"
                  >
                    <fieldset>
                      <div className="form-group">
                        <label className="col-xs-2 col-lg-3 control-label">
                          Нэр
                        </label>
                        <div className="col-xs-9 col-lg-6 inputGroupContainer">
                          <div className="input-group">
                            <input
                              type="text"
                              className="form-control"
                              name="menumn"
                            />
                          </div>
                        </div>
                      </div>
                    </fieldset>

                    <fieldset>
                      <div className="form-group">
                        <label className="col-xs-2 col-lg-3 control-label">
                          Холбоос
                        </label>
                        <div className="col-xs-9 col-lg-6 inputGroupContainer">
                          <div className="input-group">
                            <input
                              type="text"
                              className="form-control"
                              name="link"
                            />
                          </div>
                        </div>
                      </div>
                    </fieldset>

                    <fieldset>
                      <div className="form-group">
                        <label className="col-xs-2 col-lg-3 control-label">
                          Order
                        </label>
                        <div className="col-xs-9 col-lg-6 inputGroupContainer">
                          <div className="input-group">
                            <input
                              type="text"
                              className="form-control"
                              name="order"
                            />
                          </div>
                        </div>
                      </div>
                    </fieldset>

                  </form>
                {/* </BootstrapValidator> */}
              </div>
              <div className="modal-footer">
                <div className="col-md-12">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    {" "}
                    Цуцлах{" "}
                  </button>
                  <button className="btn btn-primary" type="submit">
                    {" "}
                    Хадгалах{" "}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.account,
    menu: state.menu
  };
}

// function mapDispatchToProps(dispatch) {
//   return {
//       getMenuList: t
//   };
// }

export default connect(
  mapStateToProps,
  {getMenuList}
)(menuList);
