import { encode, encrypt } from '../utils/crypt';

const axios = require('../../config/axios')
//var test = require('../utils/crypt');

export const LOGIN_USER = 'LOGIN_USER'
export const LOGOUT_USER = 'LOGOUT_USER'
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS'
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE'

export const checkToken = (token) => async dispatch => {
  console.log("checkToken", token)
  axios.get('/admin/userinfo', { headers: { 'Authorization': "Bearer " + token } }).then(res => {
    dispatch(loginUserSuccess(res.data.value))
  }).catch(err => {
    console.log(err.response);
    localStorage.removeItem('token');
  })
}

export const loginUser = (user) => async dispatch => {
  console.log(user)
  console.log("XERE")

  axios.post('/login/adminlogin', user).then(res => {
    console.log(res.data)

    if (res.data.value.access_token) {
      console.log("SUCCESS", res.data.value)
      localStorage.setItem('token', res.data.value.access_token);
      localStorage.setItem('lang', 'en');
    
      axios.defaults.headers.common['Authorization'] = "Bearer " + res.data.value.access_token;

      dispatch(loginUserSuccess(res.data.value.userInfo));
      return;
    }

    dispatch(loginUserFailure(res.data));
  })

  return;
  const request = new Request('/token', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
    body: data
  });

  return (dispatch) => {

    return fetch(request).then(response => {
      if (response.status !== 200) {
        dispatch(loginUserFailure("Invalid username or password."));
      } else {
        //response is a promise
        response.json().then(function (obj) {
          console.log(response);

          //update the data into localstorage
          let authString = encrypt(obj);
          console.log(obj, authString);
          sessionStorage.setItem('authorisationData', authString);

          dispatch(loginUserSuccess(obj));
        })
      }

    }).catch(err => {
      diapatch(loginUserFailure(err));
      throw err;
    })
  }
}

export function loginUserSuccess(user) {
  return { type: LOGIN_USER_SUCCESS, data: user }
}

export function loginUserFailure(err) {
  return { type: LOGIN_USER_FAILURE, error: "Invalid username or password" }
}

export function logoutUser() {
  sessionStorage.removeItem('authorisationData');
  return { type: LOGOUT_USER, data: {} }
}
